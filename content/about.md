---
hidden: true
---

Welcome to my webpage! Here, you will find information that I found that it
could be useful to share with the internet and I personally found exciting
enough to spent some hours investigating about. Have fun and get in touch.

## About me


My name is André Almeida, but everybody calls me
[`tony`](https://www.youtube.com/watch?v=zhl-Cs1-sG4). I'm currently based
in Latin America.

I'm a software developer, interested in free software, privacy and low level
programming. I work as a Linux kernel developer at
[Igalia](https://igalia.com).

## Contact

If you want to get in touch with me, please feel free to send a message to
my email address: andrealmeid@riseup.net

To encrypt your message, use my public key: `0XA4F4604BA7C940CE`,
[key file](/tony.asc).

_`memento mori`_

![Heart](/blog-pics/heart.png#c)
