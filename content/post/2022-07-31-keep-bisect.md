---
title: Keeping a project bisectable
subtitle: People write code. Test coverage is never enough. And we all write bugs.
image: /blog-pics/bug.jpg
date: 2022-08-04
tags: ["kernel", "git", "planet"]
---

People write code. Test coverage is never enough. Some angry contributor will
disable the CI. And we all write bugs.

---

But that's OK, it part of the job. Programming is hard and sometimes we may miss
a corner case, forget that numbers overflow and all other strange things that
computers can do.

One easy thing that we can do to help the poor developer that needs to find
what changed in the code that stopped their printer to work properly, is to keep
the project bisectable.

A "bisectable" project is a project where one can reliably run [`git
bisect`](https://git-scm.com/docs/git-bisect), which is a very useful command
to find a commit that introduces a bug. It works doing a binary search in the
git history until finding the guilty commit. This process involves building
each step of the bisect and running a test on each build to check if it's good
or bad (that you can magically automate with [`git bisect
run`](https://git-scm.com/docs/git-bisect#_bisect_run)).  The problem is, if
you can't compile, you can't tell if this commit is before or
after the bug (it can even be the culpable commit itself!). Then you need to
jump and try [another commit](https://git-scm.com/docs/git-bisect#_avoiding_testing_a_commit)
and hope that it will compile, making the process more painful. A lot of build
breakages along the commit history can easily discourage a brave bisecter.

### Keep it buildable

To make sure we keep the bisectability of a project, we just need to make sure
that __every commit__ is buildable. Sometimes we split a feature in a bunch of
cool commits but fail to see if they work as standalone changes. We can simply
do a interactive rebase, building it at each step:

```
git rebase -i main --exec "make"
```

Replace `main` with the reference that you want to build from and `make` with
your fancy build system and ta-da, let the magic happen. If any of the steps
fails to build, the rebase stops (given that "any command that fails will
interrupt the rebase, with exit code 1") so you can investigate what happened.

Of course, there's still some room for build breakages for different build
configurations, but if you always follow this practice before upstreaming your
patches you are building a better git log.

- **Discussion on [Reddit](https://www.reddit.com/r/programming/comments/wg1ql3/keeping_a_project_bisectable/).**

- **Discussion on [Hacker News](https://news.ycombinator.com/item?id=32342860)**
