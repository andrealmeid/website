This is my personal webpage :)

It was creating with the help of:

- [Hugo](https://gohugo.io), for transforming Markdown into HTML;
- [ribice](https://www.ribice.ba/), for the kiss theme;
- [Juliana Krauss](https://www.behance.net/jupkrauss), for helping me with the
design aspects.

---

### License

Unless otherwise noted, all content here is licensed as
[Creative Commons](https://creativecommons.org/licenses/by/4.0/).
